import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EncabezadoComponent } from './compomentes/encabezado/encabezado.component';
import { AcercaDeComponent } from './compomentes/acerca-de/acerca-de.component';
import { ExperienciaComponent } from './compomentes/experiencia/experiencia.component';
import { EducacionComponent } from './compomentes/educacion/educacion.component';
import { SkillsComponent } from './compomentes/skills/skills.component';
import { ProyectosComponent } from './compomentes/proyectos/proyectos.component';
import { PieComponent } from './compomentes/pie/pie.component';
// Para obtener todos los RECURSOS del SERVIDOR
import { HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    AcercaDeComponent,
    ExperienciaComponent,
    EducacionComponent,
    SkillsComponent,
    ProyectosComponent,
    PieComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
